package repository;

import java.util.Collection;

import domain.Account;
import domain.Alumno;

public interface AlumnoRepository extends BaseRepository<Alumno, Long>{
	Alumno findByIdAlumno(Long id_alumno);

	Alumno findById(Long id_alumno);
}
