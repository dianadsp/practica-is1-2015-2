
package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.AccountRepository;
import repository.AlumnoRepository;
import domain.Alumno;

@Repository
public class JpaAlumnoRepository extends JpaBaseRepository<Alumno, Long> 
implements	AlumnoRepository{

	
	@Override
	public Alumno findById(Long id_alumno) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM  a WHERE a.id_alumno = :id_alumno";
		TypedQuery<Alumno> query = entityManager.createQuery(jpaQuery, Alumno.class);
		query.setParameter("Alumno", id_alumno);
		return getFirstResult(query);
	}

	@Override
	public Alumno findByIdAlumno(Long id_alumno) {
	
		return null;
	}

		
	
}
